#!/usr/bin/env make

cp: ## Копирование файлов для laradock
	cp ./.env.example.local ./.env
	cp ./laravel-echo-server/laravel-echo-server.json.local.example ./laravel-echo-server/laravel-echo-server.json
	cp ./laravel-horizon/supervisord.d/laravel-horizon.conf.example ./laravel-horizon/supervisord.d/laravel-horizon.conf
	cp ./mysql/docker-entrypoint-initdb.d/createdb.sql.example ./mysql/docker-entrypoint-initdb.d/createdb.sql
	cp ./php-worker/supervisord.d/mail-php-worker.conf.example ./php-worker/supervisord.d/mail-php-worker.conf
	cp ./php-worker/supervisord.d/user-php-worker.conf.example ./php-worker/supervisord.d/user-php-worker.conf
	cp ./php-worker/supervisord.d/seo-php-worker.conf.example ./php-worker/supervisord.d/seo-php-worker.conf

cp-prod: ## Копирование файлов для laradock
	cp ./.env.example.prod ./.env
	cp ./laravel-echo-server/laravel-echo-server.json.production.example ./laravel-echo-server/laravel-echo-server.json
	cp ./mysql/docker-entrypoint-initdb.d/createdb.sql.example ./mysql/docker-entrypoint-initdb.d/createdb.sql
	cp ./php-worker/supervisord.d/mail-php-worker.conf.example ./php-worker/supervisord.d/mail-php-worker.conf
	cp ./php-worker/supervisord.d/user-php-worker.conf.example ./php-worker/supervisord.d/user-php-worker.conf
	cp ./php-worker/supervisord.d/seo-php-worker.conf.example ./php-worker/supervisord.d/seo-php-worker.conf

server-supervisor-help: ## Supervisor Help
	# systemctl start supervisord
	# systemctl stop supervisord
	# systemctl status supervisord
	# systemctl status supervisord
	# systemctl restart artagrad-nuxt supervisord
	# supervisor restart artagrad-nuxt
	# supervisorctl status
	# supervisorctl restart artagrad-nuxt
	# kill -s SIGTERM 5823

server-firewall-help: ## Firewall Help
	# firewall-cmd --permanent --zone=public --add-port=6010/tcp
	# firewall-cmd --permanent --zone=public --remove-port=6010/tcp
	# firewall-cmd --reload
	# firewall-cmd --list-ports

server-nginx-help: ## Nginx Help
	# systemctl start nginx
	# systemctl restart nginx
	# systemctl stop nginx
	# systemctl status nginx
	# systemctl reload nginx
	# nginx -t


---------------: ## ---------------
