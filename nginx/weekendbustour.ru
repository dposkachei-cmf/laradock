server {
    server_name weekendbustour.ru www.weekendbustour.ru;

    root /home/sites/weekendbustour.ru/public;
    client_max_body_size 32m;
    server_tokens off;

    index index.php index.html index.htm index.nginx-debian.html;

    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/weekendbustour.ru/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/weekendbustour.ru/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }
    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/var/run/php/php7.4-fpm.sock;
    }

    location ~ /\.ht {
        deny all;
    }
}
server {
    if ($host = www.weekendbustour.ru) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


    if ($host = weekendbustour.ru) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


    server_name weekendbustour.ru www.weekendbustour.ru;
    listen 80;
    return 404; # managed by Certbot




}
