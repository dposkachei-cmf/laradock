server {

    listen 80;
    listen [::]:80;

    server_name image.localhost;
    root /var/www/backend/public;
    index index.php index.html index.htm;


    location ~ \.php$ {
        try_files $uri /index.php =404;
        fastcgi_pass php-upstream;
        fastcgi_index index.php;
        fastcgi_buffers 16 16k;
        fastcgi_buffer_size 32k;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        #fixes timeouts
        fastcgi_read_timeout 600;
        include fastcgi_params;
    }
    location /storage {
        add_header 'Access-Control-Allow-Origin' '*';
        add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
        add_header 'Access-Control-Allow-Headers' 'Authorization,Origin,X-Requested-With,Content-Type,Accept,X-Auth-Pass';
        try_files $uri $uri/ /index.php$is_args$args;
    }

    location / {
        if ($request_method = 'OPTIONS') {
            add_header 'Access-Control-Allow-Origin' '*';
            add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS, DELETE, PUT';
            add_header 'Access-Control-Allow-Headers' 'Authorization,Origin,X-Requested-With,Content-Type,Accept,X-Auth-Pass';
            add_header 'Content-Type' 'text/plain; charset=utf-8';
            add_header 'Content-Length' 0;
            return 204;
        }
        try_files $uri $uri/ /index.php$is_args$args;
    }

    location ~ /\.ht {
        deny all;
    }

    location /.well-known/acme-challenge/ {
        root /var/www/letsencrypt/;
        log_not_found off;
    }

    # This block will catch static file requests, such as images
    # The ?: prefix is a 'non-capturing' mark, meaning we do not require
    # the pattern to be captured into $1 which should help improve performance
    location ~* \.(?:jpg|jpeg|gif|png|ico|xml|webp)$ {
        access_log        off;
        log_not_found     off;

        # The Expires HTTP header is a basic means of controlling caches; it tells all caches how long
        # the associated representation is fresh for. After that time, caches will always check back with
        # the origin server to see if a document is changed.
        #
        # "If a request includes the no-cache directive, it SHOULD NOT include min-fresh, max-stale, or max-age."
        # (source: http://www.ietf.org/rfc/rfc2616.txt, p114)
        #
        # Nginx automatically sets the `Cache-Control: max-age=t` header, if `expires` is present, where t is a time
        # specified in the directive, in seconds. Shortcuts for time can be used, for example 5m for 5 minutes.
        #
        expires           60m;

        # public:           marks authenticated responses as cacheable; normally, if HTTP authentication is required,
        #                   responses are automatically private.
        #
        add_header        Cache-Control "must-revalidate";
        add_header        Cache-Control "public";
    }
}
