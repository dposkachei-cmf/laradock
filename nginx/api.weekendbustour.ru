server {
    server_name api.weekendbustour.ru www.api.weekendbustour.ru;

    root /home/sites/weekendbustour.ru/public;
    client_max_body_size 32m;

    index index.php index.html index.htm index.nginx-debian.html;

    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/api.weekendbustour.ru/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/api.weekendbustour.ru/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

    location / {
        if ($request_method = 'OPTIONS') {
            add_header 'Access-Control-Allow-Origin' '*';
            add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS, DELETE';
            add_header 'Access-Control-Allow-Headers' 'Authorization,Origin,X-Requested-With,Content-Type,Accept,X-Auth-Pass';
            add_header 'Content-Type' 'text/plain; charset=utf-8';
            add_header 'Content-Length' 0;
            return 204;
        }
        try_files $uri $uri/ /index.php$is_args$args;
    }
    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/var/run/php/php7.4-fpm.sock;
    }

    location ~ /\.ht {
        deny all;
    }
}
server {
    if ($host = www.api.weekendbustour.ru) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


    if ($host = api.weekendbustour.ru) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


    server_name api.weekendbustour.ru www.api.weekendbustour.ru;
    listen 80;
    return 404; # managed by Certbot




}
